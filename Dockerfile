FROM openjdk:11.0
ADD target/mstp-data-0.0.1-SNAPSHOT.jar ./app.jar
EXPOSE 8333
ENTRYPOINT ["java", "-jar", "./app.jar"]
