package com.alacarte.mstpdata;

import com.alacarte.mstpdata.dataservice.AlphaVantageDataService;
import com.alacarte.mstpdata.models.Price;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class AlphaVantageDataServiceTests {

    @Autowired
    AlphaVantageDataService alpha;

    @BeforeEach
    public void setUp() {
        alpha = new AlphaVantageDataService();
        ReflectionTestUtils.setField(alpha, "restTemplate", new RestTemplate());
        ReflectionTestUtils.setField(alpha, "HOST", "alpha-vantage.p.rapidapi.com");
        ReflectionTestUtils.setField(alpha, "API_KEY", "3b68e4eb3fmshe03cf9ab6c3f934p1f100bjsn1c86ac052a7e");
    }

    @Test
    public void getWeekly_success() {
        List<Price> prices = alpha.getWeeklyDataFor("AAPL");
        Assertions.assertNotNull(prices);
    }

}
