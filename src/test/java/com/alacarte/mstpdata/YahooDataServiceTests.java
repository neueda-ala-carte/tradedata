package com.alacarte.mstpdata;

import com.alacarte.mstpdata.dataservice.YahooDataService;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

public class YahooDataServiceTests {

    @Autowired
    YahooDataService yahoo;

    @BeforeEach
    public void setUp() {
        yahoo = new YahooDataService();
        ReflectionTestUtils.setField(yahoo, "restTemplate", new RestTemplate());
        ReflectionTestUtils.setField(yahoo, "HOST", "apidojo-yahoo-finance-v1.p.rapidapi.com");
        ReflectionTestUtils.setField(yahoo, "API_KEY", "3b68e4eb3fmshe03cf9ab6c3f934p1f100bjsn1c86ac052a7e");
    }

    @Test
    public void getAnalysis_success() {
        JsonNode analysis = yahoo.getInsights("AAPL");
        Assertions.assertNotNull(analysis);
    }

}
