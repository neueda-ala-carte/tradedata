package com.alacarte.mstpdata.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Quote {
    String symbol;
    String companyName;
    Double latestPrice;
}
