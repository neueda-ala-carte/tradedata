package com.alacarte.mstpdata.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Recommendation {
    String shortName;
    String symbol;
}
