package com.alacarte.mstpdata.models;

import lombok.Data;

@Data
public class Price {
    String date;
    Double close;
    Long volume;

    public Price(String date, Double close, Long volume) {
        this.date = date;
        this.close = close;
        this.volume = volume;
    }
}
