package com.alacarte.mstpdata.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Report {
    String id;
    String title;
    String provider;
    String publishedOn;
    String summary;
}
