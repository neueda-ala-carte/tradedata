package com.alacarte.mstpdata.dataservice;

import com.alacarte.mstpdata.models.Story;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Data service for accessing Bloomberg for News Dashboard
 * @author anna
 */
@Service
@Lazy
public class BloombergDataService extends DataService {

    @Value("${rapidapi.host.bloomberg}")
    private String HOST;

    public List<Story> getStocksNewsData() {
        String url = String.format("https://%s/news/list?id=stocks", HOST);
        JsonNode body = getData(url, HOST);
        return deserialiseJsonObject(body);
    }

    public List<Story> getQuickTakeNewsData() {
        String url = String.format("https://%s/news/list?id=quickTake", HOST);
        JsonNode body = getData(url, HOST);
        return deserialiseJsonObject(body);
    }

    public List<Story> deserialiseJsonObject(JsonNode body) {
        JsonNode stories = body.get("modules").get(0).get("stories");
        ObjectMapper mapper = new ObjectMapper();
        List<Story> storyList = mapper.convertValue(stories, new TypeReference<List<Story>>(){});
        return storyList;
    }

}
