package com.alacarte.mstpdata.dataservice;

import com.alacarte.mstpdata.models.Quote;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Lazy
public class IEXDataService extends DataService {

    @Value("${rapidapi.host.iex}")
    private String HOST;

    public Quote fetchBookingPrice(String ticker) {
        String url = String.format("https://%s/stock/%s/book", HOST, ticker);
        JsonNode body = getData(url, HOST);
        return deserialiseJsonObject(body);
    }

    public Quote deserialiseJsonObject(JsonNode body) {
        JsonNode items = body.get("quote");
        ObjectMapper mapper = new ObjectMapper();
        Quote quote = mapper.convertValue(items, new TypeReference<Quote>(){});
        return quote;
    }
}
