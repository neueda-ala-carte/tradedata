package com.alacarte.mstpdata.dataservice;

import com.alacarte.mstpdata.models.Recommendation;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Data service for accessing Yahoo - use for market summary & popular watchlists
 * @author anna
 */
@Service
@Lazy
public class YahooDataService extends DataService {

    @Value("${rapidapi.host.yahoo}")
    private String HOST;

    public List<Recommendation> getRecommendation(String ticker) {
        String url = String.format("https://%s/stock/v2/get-recommendations?symbol=%s", HOST, ticker);
        JsonNode body = getData(url, HOST);
        return deserialiseJsonObjectToList(body);
    }

    public JsonNode getInsights(String ticker) {
        String url = String.format("https://%s/stock/v2/get-insights?symbol=%s", HOST, ticker);
        JsonNode body = getData(url, HOST);
        JsonNode analysis = body.get("finance").get("result");
        return analysis;
    }

    public List<Recommendation> deserialiseJsonObjectToList(JsonNode body) {
        JsonNode items = body.get("finance").get("result").get(0).get("quotes");
        ObjectMapper mapper = new ObjectMapper();
        List<Recommendation> recList = mapper.convertValue(items, new TypeReference<List<Recommendation>>(){});
        return recList;
    }


}
