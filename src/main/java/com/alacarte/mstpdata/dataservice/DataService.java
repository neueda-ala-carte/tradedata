package com.alacarte.mstpdata.dataservice;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class DataService {

    @Value("${rapidapi.apikey}")
    private String API_KEY;

    @Autowired
    RestTemplate restTemplate;

    JsonNode getData(String url, String HOST) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("x-rapidapi-host", HOST);
        headers.set("x-rapidapi-key", API_KEY);
        HttpEntity entity = new HttpEntity(headers);

        ResponseEntity<JsonNode> response = restTemplate.exchange(
                url, HttpMethod.GET, entity, JsonNode.class);

        return response.getBody();
    }
}
