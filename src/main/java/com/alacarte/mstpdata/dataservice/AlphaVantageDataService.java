package com.alacarte.mstpdata.dataservice;

import com.alacarte.mstpdata.models.Price;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Data service for accessing Alpha Vantage for graphing dashboards
 * @author anna
 */

@Service
@Lazy
public class AlphaVantageDataService extends DataService {

    @Value("${rapidapi.host.alphavantage}")
    private String HOST;

    public List<Price> getIntradayDataFor(String ticker) {
        String url = String.format("https://%s/query?datatype=json&output_size=compact&interval=5min&function=%s&symbol=%s",
                                    HOST, "TIME_SERIES_INTRADAY", ticker);
        JsonNode body = getData(url, HOST);
        return deserialiseJsonObject("Time Series (5min)", body);
    }

    public List<Price> getDailyDataFor(String ticker) {
        JsonNode body = getData(getUrl(ticker, "TIME_SERIES_DAILY"), HOST);
        List<Price> prices = deserialiseJsonObject("Time Series (Daily)", body);
        return prices;
    }

    public List<Price> getWeeklyDataFor(String ticker) {
        JsonNode body = getData(getUrl(ticker, "TIME_SERIES_WEEKLY"), HOST);
        return deserialiseJsonObject("Weekly Time Series", body);
    }

    public List<Price> getMonthlyDataFor(String ticker) {
        JsonNode body = getData(getUrl(ticker, "TIME_SERIES_MONTHLY"), HOST);
        return deserialiseJsonObject("Monthly Time Series", body);
    }

    public String getUrl(String ticker, String function) {
        return String.format("https://%s/query?datatype=json&symbol=%s&function=%s",
                HOST, ticker, function);
    }

    public List<Price> deserialiseJsonObject(String label, JsonNode body) {
        JsonNode items = body.get(label);
        List<Price> lst = new ArrayList<Price>();
        Iterator<String> it = items.fieldNames();
        while (it.hasNext()) {  //to get the key fields
            String key_field = it.next();
            Double close = Double.parseDouble(items.get(key_field).get("4. close").textValue());
            Long volume = Long.parseLong(items.get(key_field).get("5. volume").textValue());
            Price p = new Price(key_field, close, volume);
            lst.add(p);
        }

        return lst;
    }

}
