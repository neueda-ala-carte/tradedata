package com.alacarte.mstpdata;

import com.alacarte.mstpdata.models.Price;
import com.alacarte.mstpdata.models.Recommendation;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * This is a REST controller for querying stock & market data for Task 3
 * @author anna
 */
@RestController
@CrossOrigin(origins = "*")
public class QueryDataController {

    @Autowired
    QueryDataService queryDataService;

    /**
     * Gets the latest price of Stock
     * @param ticker
     * @return ResponseEntity
     */
    @GetMapping(value = "/real/{ticker}", produces = "application/json")
    ResponseEntity getRealLatestPrice(@PathVariable String ticker) {
        return ResponseEntity.ok(queryDataService.getLatestPrice(ticker));
    }

    /**
     * Gets the Analysis of the ticker
     * Note: This process is expensive so don't do it too much!!
     * @param ticker
     * @return
     */
    @GetMapping(value = "/analysis/{ticker}", produces = "application/json")
    ResponseEntity getMarketDataSummaryByRegion(@PathVariable String ticker) {
        JsonNode analysis = queryDataService.getAnalysis(ticker);
        return ResponseEntity.ok(analysis);
    }

    /**
     * Gets similar tickers for recommendation
     * Note: This process is expensive so don't do it too much!!
     * @param ticker
     * @return
     */
    @GetMapping(value = "/similar/{ticker}", produces = "application/json")
    ResponseEntity getTrendingMarketDataByRegion(@PathVariable String ticker) {
        List<Recommendation> recList = queryDataService.getRecommendation(ticker);
        return ResponseEntity.ok(recList);
    }

    /**
     * Gets the list of Stock News from Bloomberg
     * Note: This process is expensive so don't do it too much!!
     * @return
     */
    @GetMapping(value = "/news/stocks", produces = "application/json")
    ResponseEntity getStocksNewsList() {
        return ResponseEntity.ok(queryDataService.getStockNews());
    }

    /**
     * Gets the list of Stock News from Bloomberg
     * Note: This process is expensive so don't do it too much!!
     * @return
     */
    @GetMapping(value = "/news/general", produces = "application/json")
    ResponseEntity getGeneralNewsList() {
        return ResponseEntity.ok(queryDataService.getGeneralNews());
    }

    /**
     * Get hashmap to plot graph (intraday)
     * @param ticker
     * @return
     */
    @GetMapping(value = "/intraday/{ticker}", produces = "application/json")
    ResponseEntity getIntraDayPrices(@PathVariable String ticker) {
        List<Price> lst = queryDataService.getIntraDay(ticker);
        return ResponseEntity.ok(lst);
    }

    /**
     * Get hashmap to plot graph (daily)
     * @param ticker
     * @return
     */
    @GetMapping(value = "/daily/{ticker}", produces = "application/json")
    ResponseEntity getDailyPrices(@PathVariable String ticker) {
        List<Price> lst = queryDataService.getDaily(ticker);
        return ResponseEntity.ok(lst);
    }

    /**
     * Get hashmap to plot graph (weekly)
     * @param ticker
     * @return
     */
    @GetMapping(value = "/weekly/{ticker}", produces = "application/json")
    ResponseEntity getWeeklyPrices(@PathVariable String ticker) {
        List<Price> lst = queryDataService.getWeekly(ticker);
        return ResponseEntity.ok(lst);
    }

    /**
     * Get hashmap to plot graph (monthly)
     * @param ticker
     * @return
     */
    @GetMapping(value = "/monthly/{ticker}", produces = "application/json")
    ResponseEntity getMonthlyPrices(@PathVariable String ticker) {
        List<Price> lst = queryDataService.getMonthly(ticker);
        return ResponseEntity.ok(lst);
    }

}
