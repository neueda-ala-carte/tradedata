package com.alacarte.mstpdata;

import com.alacarte.mstpdata.dataservice.AlphaVantageDataService;
import com.alacarte.mstpdata.dataservice.BloombergDataService;
import com.alacarte.mstpdata.dataservice.IEXDataService;
import com.alacarte.mstpdata.dataservice.YahooDataService;
import com.alacarte.mstpdata.models.*;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;


/**
 * The aggregated Data service for RestController quick access
 * @author anna
 */
@Service
public class QueryDataService {

    @Autowired
    YahooDataService yahoo;

    @Autowired
    IEXDataService iex;

    @Autowired
    BloombergDataService bloomberg;

    @Autowired
    AlphaVantageDataService alpha;

    public Quote getLatestPrice(String ticker) {
        return iex.fetchBookingPrice(ticker);
    }

    public List<Story> getStockNews() {
        return bloomberg.getStocksNewsData();
    }

    public List<Story> getGeneralNews() {
        return bloomberg.getQuickTakeNewsData();
    }

    public JsonNode getAnalysis(String ticker) {
        return yahoo.getInsights(ticker);
    }

    public List<Recommendation> getRecommendation(String ticker) {
        return yahoo.getRecommendation(ticker);
    }

    public List<Price> getIntraDay(String ticker) {
        return alpha.getIntradayDataFor(ticker);
    }

    public List<Price> getDaily(String ticker) {
        return alpha.getDailyDataFor(ticker);
    }

    public List<Price> getWeekly(String ticker) {
        return alpha.getWeeklyDataFor(ticker);
    }

    public List<Price> getMonthly(String ticker) {
        return alpha.getMonthlyDataFor(ticker);
    }

}
